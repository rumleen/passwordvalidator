/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package PasswordValidator;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Main {
    public static void main(String[] args) {
        PasswordValidator pv = new PasswordValidator();
        String goodpwd = "Abcdefghij@0";
        String baddpwd = "abraham";
        String boundaryinpwd = "Aaaaaa@0";
        String boundaryoutpwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^99999999";

        System.out.println(String.format("Validation for password \'%s\' = %s", goodpwd,pv.isPasswordValid(goodpwd)));
        System.out.println(String.format("Validation for password \'%s\' = %s", goodpwd,pv.isPasswordValid(baddpwd)));
        System.out.println(String.format("Validation for password \'%s\' = %s", goodpwd,pv.isPasswordValid(boundaryinpwd)));
        System.out.println(String.format("Validation for password \'%s\' = %s", goodpwd,pv.isPasswordValid(boundaryoutpwd)));

    }
}
