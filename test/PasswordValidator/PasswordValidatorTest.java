/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package PasswordValidator;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class PasswordValidatorTest {
    
    PasswordValidator passwordvalidator;
    
    public PasswordValidatorTest() {
    }
    
    @BeforeAll
    public void setUp() throws Exception {
		passwordvalidator = new PasswordValidator();
	}
    
    @AfterAll
   public void tearDown() throws Exception {
	}
    @Test
	public void testGood1IsPasswordValid() {
        String pwd = "Abcdefghij@0";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGood2IsPasswordValid() {
        String pwd = "Bbcdefghij@1";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	@Test
	public void testGood3IsPasswordValid() {
        String pwd = "Cbcdefghij@2";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	@Test
	public void testGood4IsPasswordValid() {
        String pwd = "Dbcdefghij@3";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void testBad1IsPasswordValid() {
        String pwd = "abraham";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = false;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBad2IsPasswordValid() {
        String pwd = "123456789";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = false;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBad3IsPasswordValid() {
        String pwd = "ABRAHAM";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = false;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBad4IsPasswordValid() {
        String pwd = "!@@*@*@*";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = false;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryIn1IsPasswordValid() {
        String pwd = "Aaaaaa@0";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryIn2IsPasswordValid() {
        String pwd = "Baaaaa@1";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	@Test
	public void testBoundaryIn3IsPasswordValid() {
        String pwd = "Caaaaa@2";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	@Test
	public void testBoundaryIn4IsPasswordValid() {
        String pwd = "Daaaaa@3";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryOut1IsPasswordValid() {
        String pwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^999999999";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryOut2IsPasswordValid() {
        String pwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^999999998";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryOut3IsPasswordValid() {
        String pwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^999999997";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testBoundaryOut4IsPasswordValid() {
        String pwd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^999999996";
        boolean actual = passwordvalidator.isPasswordValid(pwd);
        boolean expected = true;
		assertEquals(expected, actual);
	}
}
